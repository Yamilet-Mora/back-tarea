
'use strict'

// Llamar las librerias de body-parser y express
const bodyParser = require('body-parser');
const express = require('express');

// Iniciamos express
const app = express();

// Activar los Cors
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin","*"),
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
});

//activar middlewares de express
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.use(require('../Routes/routes'));




module.exports = app;