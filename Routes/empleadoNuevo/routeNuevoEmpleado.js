const express = require('express');
const modelEmpleadoNuevo = require('../../Models/refaccionaria/modelNuevoEmpleado');

let app = express();

app.post('/empleado/nuevo', ( req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProducto = new modelEmpleadoNuevo({
        nombreEmpleado: body.nombreEmpleado,
        apellidoEmpleado: body.apellidoEmpleado,
        telefonoEmpleado: body.telefonoEmpleado,
        sueldoEmpleado: body.sueldoEmpleado,
        direccionEmpleado: body.direccionEmpleado,
    });

    newSchemaEmpleado
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos Guardados con Exito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                .json({
                    ok:false,
                    message: 'Error al ingresar datos',
                    err
                });
            }
        )
});

app.get('/obtener/empleado', async ( req, res) => {
    const respuesta = await modelEmpleadoNuevo.find()
    res.status(200)
        .json({
            ok:true,
            respuesta
        });
});

app.delete('/eliminar/empleado/:id', async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelEmpleadoNuevo.findByIdAndDelete(id);
    res.status(200)
        .json({
            ok:true,
            msj: "El registro se elimino correctamente".
            respuesta
        });
});

app.put('/update/empleado/:id', async ( req,res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelEmpleadoNuevo.findByIdAndUpdate(id, campos, { new: true});
    res.status(202)
        .json({
            ok: true,
            msj: "Documento actualizado correctamente",
            respuesta
        });
});

module.exports = app;