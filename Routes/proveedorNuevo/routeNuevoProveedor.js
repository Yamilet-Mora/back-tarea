const express = require('express');
const modelProveedorNuevo = require('../../Models/refaccionaria/modelNuevoProveedor');

let app = express();

app.post('/proveedor/nuevo', ( req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProveedor = new modelProveedorNuevo({
        nombreProveedor: body.nombreProveedor,
        telefonoProveedor: body.telefonoProveedor,
        correoProveedor: body.correoProveedor,
        tipoProductoProveedor: body.tipoProductoProveedor,
        statusProveedor: body.statusProveedor,
    });

    newSchemaProveedor
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos Guardados con Exito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                .json({
                    ok:false,
                    message: 'Error al ingresar datos',
                    err
                });
            }
        )
});

app.get('/obtener/proveedor', async ( req, res) => {
    const respuesta = await modelProveedorNuevo.find()
    res.status(200)
        .json({
            ok:true,
            respuesta
        });
});

app.delete('/eliminar/proveedor/:id', async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelProveedorNuevo.findByIdAndDelete(id);
    res.status(200)
        .json({
            ok:true,
            msj: "El registro se elimino correctamente".
            respuesta
        });
});

app.put('/update/proveedor/:id', async ( req,res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelProveedorNuevo.findByIdAndUpdate(id, campos, { new: true});
    res.status(202)
        .json({
            ok: true,
            msj: "Documento actualizado correctamente",
            respuesta
        });
});

module.exports = app;