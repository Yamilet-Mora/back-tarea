const express = require('express');
const modelSucursalNuevo = require('../../Models/refaccionaria/modelNuevoSucursal');

let app = express();

app.post('/sucursal/nuevo', ( req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaSucursal = new modelSucursalNuevo({
        nombreSucursal: body.nombreSucursal,
        telefonoSucursal: body.telefonoSucursal,
        direccionSucursal: body.direccionSucursal,
        correoSucursal: body.correoSucursal,
        gerenteSucursal: body.gerenteSucursal,
    });

    newSchemaSucursal
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok:true,
                        message: 'Datos Guardados con Exito',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                .json({
                    ok:false,
                    message: 'Error al ingresar datos',
                    err
                });
            }
        )
});

app.get('/obtener/sucursal', async ( req, res) => {
    const respuesta = await modelSucursalNuevo.find()
    res.status(200)
        .json({
            ok:true,
            respuesta
        });
});

app.delete('/eliminar/sucursal/:id', async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelSucursalNuevo.findByIdAndDelete(id);
    res.status(200)
        .json({
            ok:true,
            msj: "El registro se elimino correctamente".
            respuesta
        });
});

app.put('/update/sucursal/:id', async ( req,res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelSucursalNuevo.findByIdAndUpdate(id, campos, { new: true});
    res.status(202)
        .json({
            ok: true,
            msj: "Documento actualizado correctamente",
            respuesta
        });
});

module.exports = app;