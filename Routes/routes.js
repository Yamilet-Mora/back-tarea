const express = require('express');
const app = express();



app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./empleadoNuevo/routeNuevoEmpleado'));
app.use(require('./proveedorNuevo/routeNuevoProveedor'));
app.use(require('./sucursalNuevo/routeNuevoSucursal'));

module.exports = app;