const express = require('express');
const modelProductoNuevo = require('../../Models/refaccionaria/modelNuevoProducto');

let app = express();

app.post('/producto/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProducto = new modelProductoNuevo ({
        nombreProducto: body.nombreProducto,
        marcaProducto: body.marcaProducto,
        presentacionProducto: body.presentacionProducto,
        contenidoProducto: body.contenidoProducto,
        costoProducto: body.costoProducto,
        proveedorProducto: body.proveedorProducto,
        cantidadIngresa: body.cantidadIngresa,
        statusProducto: body.statusProducto,
        descripcionProducto: body.descripcionProducto,
    });

    newSchemaProducto
        .save()
        .then(
            (data) => {
                return res.status(200)
                    .json({
                        ok: true,
                        message: 'Datos guardados',
                        data
                    });
            }
        )
        .catch(
            (err) => {
                return res.status(500)
                    .json({
                        ok: false,
                        message: 'Datos no guardados',
                        err
                    });
            }
        );
});

app.get('/obtener/producto' , async ( req, res) => {
    const respuesta = await modelProductoNuevo.find()
    res.status(200)
        .json({
            ok: true,
            respuesta
        });
});

app.delete('/eliminar/producto/:id' , async ( req, res) => {
    let id = req.params.id;
    const respuesta = await modelProductoNuevo.findByIdAndDelete(id);
    res.status(200)
        .json({
            ok:true,
            msj: "Registro eliminado correctamente",
            respuesta
        });
});

app.put('/update/producto/:id' , async ( req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelProductoNuevo.findByIdAndUpdate(id,campos,{new:true});
    res.status(202)
        .json({
            ok:true,
            msj: "El documento se actualizo correctamente",
            respuesta
        });
});


module.exports = app;