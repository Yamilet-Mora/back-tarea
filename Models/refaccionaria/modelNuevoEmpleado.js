const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoEmpleadoRefaccionaria =  new Schema({
    nombreEmpleado: { type: String},
    apellidoEmpleado: { type: String},
    telefonoEmpleado: { type: Number},
    sueldoEmpleado: { type: Number},
    direccionEmpleado: {type: String},
});

module.exports = mongoose.model('nuevoEmpleado',nuevoEmpleadoRefaccionaria);
