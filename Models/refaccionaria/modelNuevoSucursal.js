const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoSucursalRefaccionaria = new Schema({
    nombreSucursal: { type: String},
    telefonoSucursal: { type: Number},
    direccionSucursal: { type: String},
    correoSucursal: { type: String},
    gerenteSucursal: { type: String},
});

module.exports = mongoose.model('nuevoSucursal',nuevoSucursalRefaccionaria);