const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let nuevoProveedorRefaccionaria = new Schema({
    nombreProveedor: { type: String},
    telefonoProveedor: { type: Number},
    correoProveedor: { type: String},
    tipoProductoProveedor: { type: String},
    statusProveedor: { type: Boolean},
});

module.exports = mongoose.model('nuevoProveedor',nuevoProveedorRefaccionaria);